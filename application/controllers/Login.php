<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{	

	public function __construct()
	{
		parent::__construct();

		$this->load->model('login_model');
		$this->load->library('form_validation');
	}


	public function index()
	{
		// Carrega a pagina de login
		//$this->session->sess_destroy();	
		
		$this->load->view('login');
	}

	public function realizar_login()
	{
		
		$this->form_validation->set_rules('login', 'Login', 'required', 
			array('required' => 'O campo login é obrigatorio'));

		$this->form_validation->set_rules('senha', 'Senha', 'required', 
			array('required' => 'O campo Senha é obrigatorio'));

		if ( !$this->form_validation->run()	)
		{
			$this->index();
			
		}
		else
		{
			$login = trim( $this->input->post('login') );
			$senha = trim( $this->input->post('senha') );

			$usuario = $this->login_model->get_usuario_login($login, $senha);

			if ($usuario)
			{
				$this->session->set_userdata('id_usuario', $usuario->id);
				redirect(base_url('home'));
			}
			else
			{
				$view_data['login_erro'] = 'Login ou senha invalidos! Favor entrar em contato com a equipe de suporte!';
				$this->load->view('login', $view_data );
	
			}

		}

		

	}


	public function realizar_logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(login));
	}
}
