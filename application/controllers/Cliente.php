<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ( !$this->session->userdata('id_usuario') )
		{
			redirect(base_url('login'));
		}

		$this->load->model('cliente_model');
	}

	public function index()
	{
		$view_data = array();

		$this->load->view('templates/html-head');
		$this->load->view('templates/menu');
		$this->load->view('pages/clientes/listar', $view_data);
		$this->load->view('templates/html-end');
	}

	public function adicionar ()
	{

		$this->load->library("form_validation");
		$response = array();

		$nome  = trim( $this->input->post('nome') );
		$email = trim( $this->input->post('email') );

		$dados = array(

			"nome" => $nome,
			"email" => $email
		);

		$this->form_validation->set_data($dados);

		$this->form_validation->set_rules('nome', '', 'required', array( "required" => "O campo Nome é obrigatório!"));

		$this->form_validation->set_rules('email', '', 'required|is_unique[tb_cliente.email]',
			array(

				"required" => "O campo Emaill é o obrigatório!",
				"valid_email" => "O email informado não está em um formato válido!",
				"is_unique" => "O email informado já existe para um cliente!"
			)
		);


		if ( !$this->form_validation->run() )
		{
			$response['sucesso'] = false;
			$response['msg'] = validation_errors();

			return $this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
		else
		{

			if ( $this->cliente_model->insert_cliente($nome, $email) )
			{
				$response['sucesso'] = true;
				$response['msg'] = "Cliente cadastrado com sucesso!";
			}
			else
			{
				$response['sucesso'] = false;
				$response['msg'] = "Encontramos um erro interno na nossa aplicação,favor entrar em contato com a equipe de suporte!";
			}

			return $this->output->set_content_type('application/json')->set_output(json_encode($response));

		}


	}


	public function ajax_get_clientes ()
	{
		$rs_clientes =  $this->cliente_model->get_clientes();

		$response['data'] = $rs_clientes;

		$this->output->set_content_type('application/json')
				->set_output(json_encode($response));
	}

	public function ajax_atualizar_cliente ()
	{

		$this->load->library("form_validation");

		$id  = $this->input->post("id");
		$nome = $this->input->post("nome");
		$email = $this->input->post("email");
		$ativo = $this->input->post("ativo");

		//$x = compact($id, $nome,$email, $ativo);

		$dados = array(

			"nome" => $nome,
			"email" => $email
		);

		$this->form_validation->set_data($dados);

		$this->form_validation->set_rules('nome', '', 'required', array( "required" => "O campo Nome é obrigatório!"));

		$this->form_validation->set_rules('email', '', "required|callback_email_unico_atualizacao[$id]",
			array(

				"required" => "O campo Email é o obrigatório!",
				"valid_email" => "O email informado não está em um formato válido!",
				"email_unico_atualizacao" => "O email informado já existe para um cliente!"
			)
		);

		$response = [];

		if ( !$this->form_validation->run() )
		{
			$response["sucesso"] = false;
			$response['msg'] = validation_errors();
		}
		else
		{
			if ( $this->cliente_model->atualizar_cliente($id, $nome, $email, $ativo ) )
			{
				$response["sucesso"] = true;
			}
			else {
				$response["sucesso"] = false;
			}
		}

		return $this->output->set_content_type("application/json")->
		set_output(json_encode($response));
	}

	public function ajax_excluir_cliente ()
	{
		$id = $this->input->post('id');

		$response = [];

		if ( $this->cliente_model->excluir_cliente($id) )
		{
			$response["sucesso"] = true;
		}
		else {
			$response["sucesso"] = false;
		}

		return $this->output->set_content_type("application/json")->
		set_output(json_encode($response));

	}


	public function email_unico_atualizacao ($email, $id)
	{
		//$id = $this->input->post('id');
		return $this->cliente_model->email_existe_atualizacao($id, $email);
	}


	public function relatorio_clientes_ativos ()
	{
		/*$dados['clientes_ativos'] = $this->cliente_model->get_clientes_ativos();

		$html = $this->load->view('pages/clientes/relatorio-ativos', $dados, TRUE );


		try {

		    $html2pdf = new Html2Pdf('P', 'A4', '');
		    $html2pdf->writeHTML($html);
		    $html2pdf->output('relatorio.pdf');
		} catch (Html2PdfException $e) {
		    $formatter = new ExceptionFormatter($e);
		    echo $formatter->getHtmlMessage();
		}*/
	}

}
