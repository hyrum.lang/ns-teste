<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		if ( !$this->session->userdata('id_usuario') )
		{
			redirect(base_url('login'));
		}
	}

	public function index()
	{

		$this->load->view('templates/html-head');
		$this->load->view('templates/menu');
		$this->load->view('home');
		$this->load->view('templates/html-end');
	}
}
