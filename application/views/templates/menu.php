<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">NS Teste</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url('clientes'); ?>"> <i class="fa fa-users"></i> Clientes</a></li>
        <!-- <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-bar-chart"></i> Relatórios
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('relatorios/clientes-ativos'); ?>">Clientes Ativos</a></li>
        </ul>
    </li> -->
      </ul>
      <ul class="nav navbar-nav navbar-right">

        <li><a href="<?php echo base_url('logout'); ?>" ><i class="fa fa-sign-out"></i> Sair</a></li>
      </ul>
    </div>
  </div>
</nav>
