<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NS Teste</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.min.css"/>


    <!-- jQuery -->
    <script src="<?php echo base_url('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script> 

    <script src="<?php echo base_url('assets/bower_components/js-cookie/src/js.cookie.js'); ?>"></script>

   


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.min.js"></script>

    
    <style type="text/css">
        
        body {
            padding-top: 70px;
        }

    </style>
   
  </head>

  <body>
    