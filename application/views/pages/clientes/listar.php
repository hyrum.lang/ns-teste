<div class="container-fluid">

  <h5><i class="fa fa-users"></i> Listagem de Clientes</h5>
  <hr>

  <div id="alerta_pagina" class="alert alert-dismissable">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

  </div>

   <div class="panel panel-default">

  		<div class="panel-heading"> <i class="fa fa-users"></i> Clientes</div>
  		<div class="panel-body " >

  		<div style="text-align: center;">
  			<a id="btn_novo_cliente" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Novo Cliente</a>
  			<!--<a href="#" class="btn btn-primary"><i class="fa fa-pdf-o"></i> </a>-->
  		</div>

  		<br>

	  	<div class="table-responsive">
	  		<table id="table_clientes" class="table table-striped" cellspacing="0" width="100%">

    		</table>
	  	</div>

  		</div>
  </div>

  <br>

</div>



<div id="modal_cliente" class="modal fade" role="dialog">
	<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Adicionar Cliente</h4>
      </div>
      <div class="modal-body">

        <div id="dados_cliente" class="row">

        	<div class="col-md-12">

        		<div id="modal_cliente_alerta" class="alert">
        		</div>


        	</div>

        	<div class="col-md-12">

        		<div class="form-group">
                    <input type="hidden" name="id_cliente" id="id_cliente" >
        			<label>Nome</label><br>
        			<input type="text" name="nome" id="md_cliente_nome" class="form-control" >
        		</div>


        	</div>

        	<div class="col-md-12">

        		<div class="form-group">
        			<label>Email</label>
        			<input type="text" name="email" id="md_cliente_email" class="form-control">
        		</div>

        	</div>

            <div id="col_cliente_ativo" class="col-md-12">

        		<div class="form-group">
        			<label>Ativo</label>
        			<select class="form-control" name="ativo" id="md_cliente_ativo">
                        <option value="true">Sim</option>
                        <option value="false">Não</option>
        			</select>
        		</div>

        	</div>

        </div>

        <div id="loading_modal_cliente" class="row">
        	<div class="col-md-12">
        		<div style="text-align:center;">
        			<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
					<span class="sr-only">Loading...</span>
				</div>
        	</div>
        </div>

      </div>
      <div class="modal-footer">

      	<button id="btn_modal_cliente_cancelar" type="button" data-dismiss="modal" class="btn btn-danger">Cancelar</button>
      	<button id="btn_modal_cliente_fechar" type="button" class="btn btn-info">Fechar</button>
      	<button id="btn_modal_cliente_adicionar" type="button" class="btn btn-primary">Adicionar</button>
        <button id="btn_modal_cliente_atualizar" type="button" class="btn btn-primary">Atualizar</button>

      </div>
    </div>

  </div>
</div>


<div id="modal_cliente_excluir" class="modal fade" role="dialog">
	<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Excluir Cliente</h4>
      </div>
      <div class="modal-body">

        <div id="modal_excluir_msg" class="">
            Você tem certeza que deseja excluir o cliente selecionado?
        </div>

        <div id="loading_modal_excluir" class="row">
        	<div class="col-md-12">
        		<div style="text-align:center;">
        			<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
					<span class="sr-only">Loading...</span>
				</div>
        	</div>
        </div>

      </div>
      <div class="modal-footer">

      	<button id="btn_cancelar_modal_excluir" type="button" data-dismiss="modal" class="btn btn-danger">Cancelar</button>
      	<button id="btn_fechar_modal_excluir" type="button" class="btn btn-info">Fechar</button>
        <button id="btn_excluir_modal_excluir" type="button" class="btn btn-primary">Excluir</button>

      </div>
    </div>

  </div>
</div>


<script type="text/javascript">

	function config_data_table ()
	{
		$('#table_clientes').DataTable({
            ordering: false,
            select: {
                style: 'single'
            },
			responsive: true,
			columns: [
						        { title: 'ID', data: 'id' },
						        { title: 'Nome', data: 'nome' },
						        { title: 'Email', data: 'email' },
						        { title: 'Ativo' , data: 'ativo' },
						        { title: 'Data Cadastro', data: 'dt_criacao'},
						        { title: 'Data Última Atualização', data: 'dt_atualizacao'}
			],
			dom: 'Bfrtip',
			buttons: [
		        {
		            extend: 'collection',
		            text: 'Exportar',
		            buttons: [ 'csv', 'excel', 'pdf' ]
		        },

                {
                    extend:'selected',
                    text: 'Editar',
                    action: function (e, dt, node, config) {

                        var dados_cliente = dt.rows({selected: true}).data()[0];
                        console.log(dados_cliente);
                        $("#id_cliente").val(dados_cliente.id);
                        $("#md_cliente_nome").val(dados_cliente.nome);
                        $("#md_cliente_email").val(dados_cliente.email);

                        $("#md_cliente_ativo").val( dados_cliente.ativo == 'Sim' ? 'true' : 'false' );

                         $("#btn_modal_cliente_atualizar").on('click' , function(event) {
                             $("#modal_cliente_alerta").removeClass('alert-danger alert-success alert-info alert-warning');
                             $("#loading_modal_cliente").show();
                             $("#btn_modal_cliente_atualizar").prop('disabled', true);


                             $.ajax({
                                 url: "/ajax_atualizar_cliente",
                                 type: 'post',
                                 data : {
                                     id: $("#id_cliente").val(),
                                     nome: $("#md_cliente_nome").val(),
                                     email: $("#md_cliente_email").val(),
                                     ativo: $("#md_cliente_ativo").val(),
                                     'csrf_token': Cookies.get("csrf_cookie")
                                 },
                                 success: function (result)
                                 {

                                     if (result.sucesso)
                                     {
                                         $("#loading_modal_cliente").hide();
                                         $("#dados_cliente").show();
                                         //$("#dados_add_cliente").children().prop("disabled",true)
                                         $("#btn_modal_cliente_fechar").show();
                                         $("#btn_modal_cliente_adicionar").hide();
                                         $("#btn_modal_cliente_cancelar").hide();
                                         $("#btn_modal_cliente_atualizar").hide();

                                         $("#modal_cliente_alerta").addClass("alert-success");
                                         $("#modal_cliente_alerta").html('Cliente atualizado com sucesso!');
                                         $("#modal_cliente_alerta").show();
                                     }
                                     else
                                     {
                                         $("#loading_modal_cliente").hide();
                                         $("#dados_cliente").show();

                                         $("#btn_modal_cliente_adicionar").prop("disabled",false)


                                         $("#modal_cliente_alerta").addClass("alert-danger");
                                         $("#modal_cliente_alerta").html(result.msg);
                                         $("#modal_cliente_alerta").show();
                                         $("#btn_modal_cliente_atualizar").prop('disabled', false);
                                         $("#btn_modal_cliente_cancelar").show();
                                     }


                                 },
                                 error: function ()
                                 {
                                     $("#loading_modal_cliente").hide();
                                     $("#dados_cliente").show();

                                     $("#btn_modal_cliente_adicionar").hide();
                                     $("#btn_modal_cliente_cancelar").hide();
                                     $("#btn_modal_cliente_atualizar").hide();


                                     $("#modal_cliente_alerta").addClass("alert-danger");
                                     $("#modal_cliente_alerta").html('Erro ao atualizar cliente!');
                                     $("#modal_cliente_alerta").show();

                                     $("#btn_modal_cliente_fechar").show();
                                 }
                             });

                        });


                        abrir_modal_cliente("editar");
                    }
                },

                {
                    extend:'selected',
                    text: 'Deletar',
                    action: function (e, dt, node, config) {
                        var dados_cliente = dt.rows({selected: true}).data()[0];
                        abrir_modal_excluir_cliente(dados_cliente);

                        $("#btn_fechar_modal_excluir").on('click', function(){

                            ajax_get_all_clientes();
                            $("#modal_cliente_excluir").modal("hide");

                        })

                        $("#btn_excluir_modal_excluir").on('click' , function(event) {

                            var dados_cliente = dt.rows({selected: true}).data()[0];
                            $("#loading_modal_excluir").show();
                            $("#btn_excluir_modal_excluir").prop('disabled', true);

                            $.ajax({
                                url: "/ajax_excluir_cliente",
                                type: 'post',
                                data : {
                                    id: dados_cliente.id,
                                    'csrf_token': Cookies.get("csrf_cookie")
                                },
                                success: function (result)
                                {

                                    if (result.sucesso)
                                    {
                                        $("#modal_excluir_msg").html("Cliente excluido com sucesso!");
                                        $("#btn_cancelar_modal_excluir").hide();
                                        $("#btn_fechar_modal_excluir").show();
                                        $("#loading_modal_excluir").hide();
                                    }
                                    else
                                    {
                                        $("#modal_excluir_msg").html("Encontramos um erro ao tentar excluir o clinte! Entre em contato com a nossa equipe de suporte!");
                                        $("#btn_cancelar_modal_excluir").hide();
                                        $("#btn_excluir_modal_excluir").hide();
                                        $("#btn_fechar_modal_excluir").show();
                                        $("#loading_modal_excluir").hide();
                                    }


                                },
                                error: function ()
                                {
                                    $("#modal_excluir_msg").html("Encontramos um erro ao tentar excluir o clinte! Entre em contato com a nossa equipe de suporte!");
                                    $("#btn_cancelar_modal_excluir").hide();
                                    $("#btn_excluir_modal_excluir").hide();
                                    $("#btn_fechar_modal_excluir").show();
                                    $("#loading_modal_excluir").hide();
                                }
                            });

                        })
                    }
                }
		    ]
		});
	}

    function abrir_modal_excluir_cliente (obj_cliente)
    {
        var str = "Você tem certeza que deseja excluir o clinete " + obj_cliente.nome + " (id: " + obj_cliente.id +")";
        $("#modal_excluir_msg").html(str);
        $("#btn_fechar_modal_excluir").hide();
        $("#loading_modal_excluir").hide();
        $("#modal_cliente_excluir").modal({backdrop: 'static'});
    }

    function abrir_modal_cliente(modo)
    {
        event.preventDefault();
        $("#loading_modal_cliente").hide();

        $("#modal_cliente_alerta").hide();
        $("#modal_cliente_alerta").html("");
        $("#modal_cliente_alerta").removeClass('alert-danger alert-success alert-info alert-warning');



        $("#col_cliente_ativo").hide();

        $("#btn_modal_cliente_cancelar").show();
        $("#btn_modal_cliente_fechar").hide();

        var btn_atualizar = $("#btn_modal_cliente_atualizar");
        var btn_adicionar = $("#btn_modal_cliente_adicionar");

        if ( modo == 'editar' )
        {
            $("#col_cliente_ativo").show();
            btn_adicionar.hide();
            btn_atualizar.prop('disabled', false).show();
        }
        else if ( modo == "inserir")
        {
            $("#md_cliente_nome").val("");
            $("#md_cliente_email").val("");
            btn_adicionar.prop('disabled', false).show();
            btn_atualizar.hide();
        }

        $("#modal_cliente").modal({backdrop: 'static'});
    }

	function ajax_get_all_clientes()
	{

		var lista_clientes = null;

		$.ajax({
			//async: false,
			url: "/ajax-get-clientes",
			type:"post",
			data: {
				'csrf_token': Cookies.get("csrf_cookie")
			},

			success: function (result)
			{

				var data_table = $("#table_clientes").DataTable();
				data_table.clear();
				data_table.rows.add(result.data);
				data_table.draw();
			},

			error: function()
			{
				$("#modal_cliente_alerta").addClass("alert-danger");
				$("#modal_cliente_alerta").html("Erro interno! Entrar em contato com a equipe de suporte!");
				$("#modal_cliente_alerta").show();
			}

		});
	}

	function config_alerta_pagina_load()
	{
		// Esconder alerta global da pagina
		$("#alerta_pagina").hide();
		$("#alerta_pagina").removeClass('alert-danger alert-success alert-info alert-warning');
	}

	function btn_novo_cliente_on_click()
	{
		$("#btn_novo_cliente").on('click', function(event){
            abrir_modal_cliente("inserir");
		});
	}


	function btn_modal_cliente_fechar_on_click()
	{
		$("#btn_modal_cliente_fechar").on('click', function(event){

			$("#modal_cliente_alerta").removeClass('alert-danger alert-success alert-info alert-warning');
			$("#modal_cliente").modal("hide");

			ajax_get_all_clientes();

		});
	}


	function btn_modal_cliente_adicionar_on_click ()
	{

		$("#btn_modal_cliente_adicionar").on('click', function(event) {

			$(this).prop("disabled",true);
			$("#dados_cliente").hide();
			$("#loading_modal_cliente").show();
            $("#btn_modal_cliente_atualizar").hide();
			$("#btn_modal_cliente_cancelar").hide();

			$("#btn_modal_cliente_adicionar").show();

			$("#modal_cliente_alerta").removeClass("alert-danger alert-success");
			$("#modal_cliente_alerta").html("");
			$("#modal_cliente_alerta").hide()

			$.ajax({
				url:"clientes/adicionar",
				data: { nome: $("#md_cliente_nome").val() ,
						email: $("#md_cliente_email").val() ,
						'csrf_token': Cookies.get("csrf_cookie")
					} ,
				type: "post",
				success: function(result) {

					if ( result.sucesso == true )
					{
						$("#loading_modal_cliente").hide();
						$("#dados_cliente").show();
						//$("#dados_add_cliente").children().prop("disabled",true)
						$("#btn_modal_cliente_fechar").show();
						$("#btn_modal_cliente_adicionar").hide();
                        $("#btn_modal_cliente_cancelar").hide();

						$("#modal_cliente_alerta").addClass("alert-success");
						$("#modal_cliente_alerta").html(result.msg);
						$("#modal_cliente_alerta").show();

					}
					else
					{
						$("#loading_modal_cliente").hide();
						$("#dados_cliente").show();

						$("#btn_modal_cliente_adicionar").prop("disabled",false)


						$("#modal_cliente_alerta").addClass("alert-danger");
						$("#modal_cliente_alerta").html(result.msg);
						$("#modal_cliente_alerta").show();

						$("#btn_modal_cliente_cancelar").show();


					}

				},

				error: function ()
				{
                    $("#modal_cliente_alerta").removeClass("alert-danger alert-success");
					$("#btn_modal_cliente_cancelar").addClass("alert-danger");
					$("#btn_modal_cliente_cancelar").html("Erro interno! Entrar em contato com a equipe de suporte!");
					$("#btn_modal_cliente_cancelar").show();
				}
			});

		});

	}

	function btn_modal_cliente_cancelar_on_click ()
	{
		$("#modal_novo_cliente").modal("hide");

		ajax_get_all_clientes();
	}


</script>

<script type="text/javascript">

	$(document).ready(function () {

		config_alerta_pagina_load();

		config_data_table();

		ajax_get_all_clientes();

		btn_novo_cliente_on_click();

		btn_modal_cliente_cancelar_on_click();

		btn_modal_cliente_fechar_on_click();

        btn_modal_cliente_adicionar_on_click();



	});

</script>
