<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NS Teste </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->

    <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">

    <!-- NProgress -->
      <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
    
    <!-- Animate.css -->
     <link href="<?php echo base_url('assets/bower_components/gentelella/vendors/animate.css/animate.min.css') ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
     <link href="<?php echo base_url('assets/bower_components/gentelella/build/css/custom.min.css') ?>" rel="stylesheet">

  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php echo form_open(base_url('login')) ; ?>

             

              <h1>NS Teste - Login</h1>

               <?php if ( validation_errors() ): ?>
                  <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                  </div>
              <?php endif ?>

              <?php if ( isset($login_erro) ): ?>
                  <div class="alert alert-danger">
                    <?php echo $login_erro ?>
                  </div>
              <?php endif ?>

              <div>
                <input type="text" name="login" class="form-control" placeholder="Login"  />
              </div>
              <div>
                <input type="password" name="senha" class="form-control" placeholder="Senha"  />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit pull-right"> Entrar</button>
                <!--<a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <!-- <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div> -->
             </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
