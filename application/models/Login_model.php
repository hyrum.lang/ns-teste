<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Login_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}


	// Verifica se existe na base o usuário, recebendo o login e a senha
	public function get_usuario_login($login, $senha)
	{
		/*
			Usando md5 por simplicidade
		*/
		$busca_usuario_query = $this->db->select('id, nome')
		->from('tb_usuario')
		->where('login', $login)
		->where('senha', md5($senha) )
		->where('ativo', 'true');

		$rs_usuario = $busca_usuario_query->get()->result();

		if ( count($rs_usuario) == 1 )
		{
			return $rs_usuario[0];
		}
		else
		{
			return NULL;
		}
	}
}