<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Cliente_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	// Verifica se existe na base o usuário, recebendo o login e a senha
	public function get_clientes()
	{
		/*
			Usando md5 por simplicidade
		*/


		$busca_clientes= $this->db->select(
			"id,
			nome,
			email,
			case
				when ativo = true then 'Sim'
				else 'Não'
			end as ativo,
			to_char(dt_criacao, 'dd/mm/YYYY HH24:MI') as dt_criacao,
			to_char(dt_atualizacao, 'dd/mm/YYYY HH24:MI') as dt_atualizacao")
		->from('tb_cliente')
		->order_by('nome');

		$rs_usuarios = $busca_clientes->get()->result();

		return $rs_usuarios;
	}

	public function insert_cliente ($nome, $email)
	{

		$dados_insert['nome'] = $nome;
		$dados_insert['email'] = $email;

		return $this->db->insert('tb_cliente', $dados_insert );
	}

	public function atualizar_cliente ($id, $nome, $email, $ativo)
	{
		$dados_cliente = [];
		$dados_cliente['nome'] = $nome;
		$dados_cliente['email'] = $email;
		$dados_cliente['ativo'] = $ativo;
		$dados_cliente['dt_atualizacao'] = "now()";

		$this->db->where('id', $id);
		return $this->db->update('tb_cliente', $dados_cliente);
	}

	public function excluir_cliente ($id)
	{

		return $this->db->delete('tb_cliente', array( "id" => $id ));
	}

	public function email_existe($email)
	{
		$rs_email = $this->db->select('id')
		->from('tb_cliente')
		->where('email',$email)
		->get()->result();

		if (count($rs_email) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function email_existe_atualizacao($id, $email)
	{
		$rs_email = $this->db->select('id')
		->from('tb_cliente')
		->where('email',$email)
		->where('id <> ', $id)
		->get()->result();

		if (count($rs_email) > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}


	public function get_clientes_ativos()
	{
		/*
			Usando md5 por simplicidade
		*/


		$busca_clientes= $this->db->select(
			"id,
			nome,
			email,
			case
				when ativo = true then 'Sim'
				else 'Não'
			end as ativo,
			to_char(dt_criacao, 'dd/mm/YYYY HH24:MI') as dt_criacao,
			to_char(dt_atualizacao, 'dd/mm/YYYY HH24:MI') as dt_atualizacao")
		->from('tb_cliente')
		->where('ativo', 'true')
		->order_by('nome');

		$rs_usuarios = $busca_clientes->get()->result();

		return $rs_usuarios;
	}

}
