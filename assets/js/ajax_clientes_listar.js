
/*
	
	Get all clientes
	
*/

function ajax_get_all_clientes()
{
	console.log(window.location.hostname);

	var lista_clientes = null;

	$.ajax({
		//async: false,
		url: "/ajax-get-clientes",
		type:"post",
		data: {
			'csrf_token': Cookies.get("csrf_cookie")
		},

		success: function (result)
		{
			lista_clientes = result;
		},

		error: function()
		{
			lista_clientes = null;
		}

	});

	return lista_clientes;
}
